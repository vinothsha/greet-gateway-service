type LoadRouteCallback func(r *gin.RouterGroup, routeRegisterType corelib.RouteRegisterMiddlewareType)

//InitializeRouter ...
func InitializeRouter(r *gin.Engine, routeGroup string, loadRouteCallback LoadRouteCallback) {
