package internal

import (
	"log"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/vinothsha/types/v1"
)

type hand struct {
	repo Service
}

func (h *hand) Greet(c *gin.Context) {
	res, err := h.repo.UserGreeting(c, &v1.WelcomeRequest{})
	if err != nil {
		log.Fatalln(err)
	}
	c.JSON(200, res)
}

func NewHandler(r Service) Handler {
	return &hand{r}
}
