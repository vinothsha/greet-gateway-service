package config

import (
	"gateway/internal"
	"gateway/internal/bye"
)

type RemoteService struct {
	Greet_Service internal.Service
	Bye_Service   bye.Service
}
