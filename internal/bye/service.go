package bye

import (
	"context"

	v1 "gitlab.com/vinothsha/types/v1"
)

type Service interface {
	ByeUser(context.Context, *v1.ByeRequest) (interface{}, error)
}
