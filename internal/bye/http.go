package bye

import (
	"log"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/vinothsha/types/v1"
)

type hand struct {
	ser Service
}

func (h *hand) ByeUser(c *gin.Context) {
	res, err := h.ser.ByeUser(c, &v1.ByeRequest{})
	if err != nil {
		log.Fatalln(err)
	}
	c.JSON(200, res)
}

func NewHandler(s Service) Handler {
	return &hand{s}
}
