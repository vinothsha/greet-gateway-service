package bye

import (
	"context"
	"log"

	v1 "gitlab.com/vinothsha/types/v1"
)

type usecase struct {
	client v1.ByeServiceClient
}

func (u *usecase) ByeUser(ctx context.Context, req *v1.ByeRequest) (interface{}, error) {
	res, err := u.client.ByeToUser(ctx, req)
	if err != nil {
		log.Fatalln(err)
	}
	return res, nil
}

func NewService(c v1.ByeServiceClient) *usecase {
	return &usecase{c}
}
