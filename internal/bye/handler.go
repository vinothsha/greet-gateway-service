package bye

import "github.com/gin-gonic/gin"

type Handler interface {
	ByeUser(*gin.Context)
}
