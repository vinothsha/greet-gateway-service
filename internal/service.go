package internal

import (
	"context"

	v1 "gitlab.com/vinothsha/types/v1"
)

type Service interface{
	UserGreeting(ctx context.Context,req *v1.WelcomeRequest)(interface{},error)
}