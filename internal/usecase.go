package internal

import (
	"context"

	v1 "gitlab.com/vinothsha/types/v1"
)

type PbClient struct {
	C v1.WelcomeServiceClient
}

func (cl PbClient) UserGreeting(ctx context.Context, req *v1.WelcomeRequest) (interface{}, error) {
	res, err := cl.C.UserGreeting(ctx, req)
	if err != nil {
		return nil, err
	}
	return res, nil

}

func NewService(c v1.WelcomeServiceClient) *PbClient {
	return &PbClient{c}
}
