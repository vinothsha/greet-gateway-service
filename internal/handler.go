package internal

import (
	"github.com/gin-gonic/gin"
)

type Handler interface {
	Greet(g *gin.Context)
}
