package main

import (
	"context"
	"fmt"
	"gateway/internal"
	"gateway/internal/bye"
	"gateway/internal/config"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/rs/cors"

	"github.com/gin-gonic/gin"
	v1 "gitlab.com/vinothsha/types/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type CloseCallBack func()

type RegisterRoute func(router *gin.Engine)

type Core struct {
	GrpcServer *grpc.Server
}
type CoreBuilder struct {
	Core *Core
}

func main() {
	cb := &CoreBuilder{}
	services, closed, err := initService(context.Background())
	if err != nil {
		log.Fatalf("%v", err)
	}
	cb.ServeHTTP(func(r *gin.Engine) {
		ResgisterRouter(r, &services)
	}, "8080", closed)
	cb.TerminateServersGracefully()
}

func initService(ctx context.Context) (config.RemoteService, func(), error) {
	var closing []*grpc.ClientConn
	conn, err := grpc.DialContext(ctx, "localhost:9080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Println(err)
	}
	closing = append(closing, conn)
	c := v1.NewWelcomeServiceClient(conn)
	conn1, err := grpc.DialContext(ctx, "localhost:9081", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Println(err)
	}
	closing = append(closing, conn1)
	c1 := v1.NewByeServiceClient(conn1)
	return config.RemoteService{Greet_Service: internal.NewService(c), Bye_Service: bye.NewService(c1)},
		func() {
			for _, co := range closing {
				co.Close()
			}
		}, nil
}

func StartHttpServer(registerRoute RegisterRoute, lPort string, closeCallBack CloseCallBack) {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	// Recovery middleware
	router.Use(gin.Recovery())
	registerRoute(router)
	//cors setting, default allows all -> *
	allowedHeaders := []string{"*"}
	allowedOrigins := []string{"*"}
	allowedMethods := []string{"GET", "POST", "OPTIONS", "DELETE"}

	corsOption := cors.New(cors.Options{
		AllowedOrigins: allowedOrigins,
		AllowedHeaders: allowedHeaders,
		AllowedMethods: allowedMethods,
	})

	handler := corsOption.Handler(router)

	srv := &http.Server{
		Addr:         lPort,
		Handler:      handler,
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}

	// Start the server
	// go func() {
	// if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
	// 	log.Fatalf("failed to serve")
	// }
	// }()

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("failed to serve")
	}
	log.Fatalf(fmt.Sprintf("Server started at %s", lPort))

}

func (cb *CoreBuilder) TerminateServersGracefully() {
	done := make(chan os.Signal)
	go signal.Notify(done, os.Interrupt)

	<-done
	// Stop the server
	grpcServer := cb.Core.GrpcServer
	if grpcServer != nil {
		cb.Core.GrpcServer.GracefulStop()
	}
	log.Fatalf("Grpc Server Exited Properly")
}

// ServeHTTP serve for HTTP methods
func (cb *CoreBuilder) ServeHTTP(registerRoute RegisterRoute, spec string, closeCallBack CloseCallBack) *CoreBuilder {
	lPort := fmt.Sprintf(":%s", spec)
	StartHttpServer(registerRoute, lPort, closeCallBack)
	return cb
}

func GreetRouter(r *gin.RouterGroup, version string, s internal.Service) {
	handler := internal.NewHandler(s)
	p := r.Group(version)
	p.POST("/greet", handler.Greet)
}

func ByeRouter(r *gin.RouterGroup, version string, s bye.Service) {
	handler := bye.NewHandler(s)
	p := r.Group(version)
	p.POST("/bye", handler.ByeUser)
}

// LoadRouteCallback ...
type LoadRouteCallback func(r *gin.RouterGroup)

// InitializeRouter ...
func InitializeRouter(r *gin.Engine, routeGroup string, loadRouteCallback LoadRouteCallback) {
	group := r.Group(routeGroup)
	loadRouteCallback(group)
}

func ResgisterRouter(r *gin.Engine, remo_service *config.RemoteService) {
	InitializeRouter(r, config.RouterGroup,
		func(r *gin.RouterGroup) {
			GreetRouter(r, config.Api_Version, remo_service.Greet_Service)
			ByeRouter(r, config.Api_Version, remo_service.Bye_Service)
		})

}
